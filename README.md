# GitLab Role

Ansible Role for a hassle-free GitLab VM on Linux.

## Example Playbook

```yml
- hosts: gitlab
  vars_files:
    - vars/main.yml
  roles:
    - { role: gitlab-role }
```

Minimum `vars/main.yml`:

```yml
gitlab_domain: "gitlab.host.TLD"
```

```yml
gitlab_domain: "gitlab.host.TLD"
gitlab_letsencrypt_enable: true
gitlab_letsencrypt_contact_emails: ["mail@host.TLD"]
gitlab_redirect_http_to_https: true
```

This will install the latest version of gitlab-ce for you including lets encrypt certificates (via HTTP challange).

You can customize your variables further to your needs.

## GitLab Settings

You can set all GitLab variables you need even if they are not predefined in this role the following way.

```yml
gitlab_extra_settings:
  - gitlab_rails:
      - key: "smtp_ca_path"
        value: "/etc/ssl/certs"
      - key: "smtp_ca_file"
        value: "/etc/ssl/certs/ca-certificates.crt"
  - mattermost:
      - key: "enable"
        value: "true"
```

GitLab Omnibus Documentation: <https://docs.gitlab.com/omnibus/>

You can also find a gitlab.rb template here: <https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template>
